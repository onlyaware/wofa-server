package com.onlyxiahui.wofa.server.net.core.session;

import com.onlyxiahui.framework.net.session.SocketSession;

/**
 * 
 * Description <br>
 * Date 2019-07-21 17:55:15<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public interface SocketSessionHandler {

	public void sessionPut(SocketSession socketSession);

	public boolean sessionHas(String key);

	public void sessionRemove(SocketSession socketSession);

	public void dataException(SocketSession socketSession, Object data, Exception e);

	public void dataWrite(SocketSession socketSession, Object data);
}
