package com.onlyxiahui.wofa.server.net.core;

/**
 * 
 * Date 2019-01-07 21:57:37<br>
 * Description
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */
public interface Server {

	/**
	 * Date 2019-01-07 21:57:45<br>
	 * Description 启动服务
	 * 
	 * @author XiaHui<br>
	 * @return
	 * @since 1.0.0
	 */
	public boolean start(int port);
}
