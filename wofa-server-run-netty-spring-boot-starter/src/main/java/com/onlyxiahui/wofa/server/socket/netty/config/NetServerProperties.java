
package com.onlyxiahui.wofa.server.socket.netty.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Description <br>
 * Date 2020-04-13 11:11:50<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
@ConfigurationProperties("spring.wofa.server.net.netty")
public class NetServerProperties {

	private List<TcpServerData> tcpServers = new ArrayList<>();
	private List<WebSocketServerData> webSocketServers = new ArrayList<>();

	public List<TcpServerData> getTcpServers() {
		return tcpServers;
	}

	public void setTcpServers(List<TcpServerData> tcpServers) {
		this.tcpServers = tcpServers;
	}

	public List<WebSocketServerData> getWebSocketServers() {
		return webSocketServers;
	}

	public void setWebSocketServers(List<WebSocketServerData> webSocketServers) {
		this.webSocketServers = webSocketServers;
	}
}
