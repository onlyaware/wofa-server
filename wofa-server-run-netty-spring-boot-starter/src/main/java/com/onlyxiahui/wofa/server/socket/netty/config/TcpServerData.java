
package com.onlyxiahui.wofa.server.socket.netty.config;

/**
 * Description <br>
 * Date 2020-04-13 11:15:48<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class TcpServerData extends BaseServerData {

	private int port = 9100;

	public TcpServerData() {
		super();
	}

	public TcpServerData(int port) {
		super();
		this.port = port;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
}
