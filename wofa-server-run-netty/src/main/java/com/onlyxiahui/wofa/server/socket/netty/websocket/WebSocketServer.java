package com.onlyxiahui.wofa.server.socket.netty.websocket;

//import java.security.cert.CertificateException;

//import javax.net.ssl.SSLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.onlyxiahui.framework.action.dispatcher.general.GeneralMessageHandler;
import com.onlyxiahui.wofa.server.net.core.Server;
import com.onlyxiahui.wofa.server.net.core.session.SocketSessionHandler;
import com.onlyxiahui.wofa.server.socket.netty.websocket.impl.WebSocketServerInitializer;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.ssl.SslContext;
//import io.netty.handler.ssl.SslContextBuilder;
//import io.netty.handler.ssl.util.SelfSignedCertificate;

/**
 *
 * @author XiaHui
 */
public final class WebSocketServer implements Server {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private int port = 9000;
	private ServerBootstrap server = new ServerBootstrap();
	private boolean start = false;
	private String websocketPath;

	private GeneralMessageHandler messageHandler;
	private SocketSessionHandler sessionHandler;
	private SslContext sslContext;

	public WebSocketServer(
			String websocketPath,
			GeneralMessageHandler messageHandler,
			SocketSessionHandler sessionHandler) {
		this(websocketPath, messageHandler, sessionHandler, null);
	}

	public WebSocketServer(
			String websocketPath,
			GeneralMessageHandler messageHandler,
			SocketSessionHandler sessionHandler,
			SslContext sslContext) {
		this.websocketPath = websocketPath;
		this.messageHandler = messageHandler;
		this.sessionHandler = sessionHandler;
		this.sslContext = sslContext;
	}

	public boolean start(int port) {
		this.port = port;
		if (start) {
			return start;
		}
		// Configure SSL.
//		SslContext sslCtx = null;
//		if (SSL) {
//			SelfSignedCertificate ssc;
//			try {
//				ssc = new SelfSignedCertificate();
//				sslCtx = SslContextBuilder.forServer(ssc.certificate(), ssc.privateKey()).build();
//			} catch (CertificateException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (SSLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		} else {
//			sslCtx = null;
//		}

		EventLoopGroup bossGroup = new NioEventLoopGroup(1);
		EventLoopGroup workerGroup = new NioEventLoopGroup();
		try {
			start = true;
			server.group(bossGroup, workerGroup);
			server.channel(NioServerSocketChannel.class);
			server.handler(new LoggingHandler(LogLevel.INFO));
			server.childHandler(new WebSocketServerInitializer(websocketPath, messageHandler, sessionHandler, sslContext));
			// server.bind(port);
			Channel ch = server.bind(port).sync().channel();
			ch.closeFuture().sync();
		} catch (InterruptedException ex) {
			start = false;
			logger.error("WebSocketServer启动失败", ex);
		} finally {
			bossGroup.shutdownGracefully();
			workerGroup.shutdownGracefully();
		}
		return start;
	}

	public int getPort() {
		return port;
	}
}
