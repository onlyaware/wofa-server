package com.onlyxiahui.wofa.server.socket.netty.common.handler;

import com.onlyxiahui.framework.action.dispatcher.extend.ActionResponse;
import com.onlyxiahui.framework.action.dispatcher.extend.ArgumentBox;
import com.onlyxiahui.framework.action.dispatcher.general.GeneralMessageHandler;
import com.onlyxiahui.framework.net.session.SocketSession;
import com.onlyxiahui.wofa.server.net.core.session.SocketSessionHandler;

/**
 * <br>
 * Date 2020-09-28 18:00:29<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class SessionDataHandler {

	private GeneralMessageHandler messageHandler;
	private SocketSessionHandler sessionHandler;

	public SessionDataHandler(
			SocketSessionHandler sessionHandler,
			GeneralMessageHandler messageHandler) {
		this.messageHandler = messageHandler;
		this.sessionHandler = sessionHandler;
	}

	public void message(SocketSession socketSession, Object data) {
		try {
			String path = "";
			ArgumentBox argumentBox = messageHandler.getArgumentBox();

			ActionResponse actionResponse = new ActionResponse() {

				@Override
				public void write(Object data) {
					SessionDataHandler.this.write(socketSession, data);
				}
			};
			argumentBox.put(ActionResponse.class, actionResponse);
			argumentBox.put(SocketSession.class, socketSession);
			Object result = messageHandler.doMessage(path, data, actionResponse, argumentBox);
			write(socketSession, result);
		} catch (Exception e) {
			sessionHandler.dataException(socketSession, data, e);
		}
	}

	public void write(SocketSession socketSession, Object object) {
		if (null != socketSession && null != object) {
			sessionHandler.dataWrite(socketSession, object);
		}
	}
}
