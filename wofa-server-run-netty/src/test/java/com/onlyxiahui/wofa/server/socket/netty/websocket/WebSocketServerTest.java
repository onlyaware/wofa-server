package com.onlyxiahui.wofa.server.socket.netty.websocket;

import com.onlyxiahui.framework.action.dispatcher.general.GeneralMessageHandler;
import com.onlyxiahui.wofa.server.net.core.session.SocketSessionHandler;
import com.onlyxiahui.wofa.server.socket.netty.common.util.SslContextUtil;
import com.onlyxiahui.wofa.server.socket.netty.websocket.action.AllAction;

import io.netty.handler.ssl.SslContext;

/**
 * Description <br>
 * Date 2021-01-08 15:36:03<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class WebSocketServerTest {

	public static void main(String[] args) {
		try {
			String certificatePath = "classpath:key/server.crt";
			String keyPath = "classpath:key/server_pkcs8.key";
			String keyPassword = null;

			SslContext sslContext = SslContextUtil.createSslContext(certificatePath, keyPath, keyPassword);
			WofaActionMessageResolverImpl amr = new WofaActionMessageResolverImpl();
			GeneralMessageHandler messageHandler = new GeneralMessageHandler();

			messageHandler.setActionMessageResolver(amr);
			messageHandler.getActionDispatcher().cover(AllAction.class);
			SocketSessionHandler sessionHandler = new SocketSessionHandlerImpl();
			WebSocketServer wss = new WebSocketServer("websocket", messageHandler, sessionHandler, sslContext);

			wss.start(9000);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
