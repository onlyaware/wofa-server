
package com.onlyxiahui.wofa.server.socket.netty.websocket.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.onlyxiahui.common.annotation.parameter.Define;
import com.onlyxiahui.framework.action.dispatcher.annotation.ActionMapping;

/**
 * Description <br>
 * Date 2020-12-26 16:33:53<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
@ActionMapping
public class AllAction {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	@ActionMapping(value = "/**/**")
	public void auth(
			@Define() String data) {
		System.out.println(data);
	}
}
